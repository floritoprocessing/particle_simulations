import java.util.Vector;


// PRESET VARIABLES:
int nrOfFluffs=500;
double[] ALL_AREA_SIZE={300,300,300};

// internal variables:
Fluff[] fluff;
CameraMoveable myCamera;
SpaceData spaceData;



void setup() {
  size(720,576,P3D);
  colorMode(RGB,255);
  myCamera=new CameraMoveable(ALL_AREA_SIZE,300);
  initFluffs(ALL_AREA_SIZE);
  spaceData=new SpaceData(ALL_AREA_SIZE,50);
  
  startFluffs(timeInSeconds());
  background(0,0,0);
}



void draw() {
  background(0,0,0);
  updateFluffs(timeInSeconds());
  
  spaceData.clearSpaceHolder();
  for (int i=0;i<fluff.length;i++) {
    spaceData.addToSpaceHolder(fluff[i]);
  }
  
  
  myCamera.update();
  
  myCamera.make(0);
    
  //spaceData.toScreen();
  //spaceData.showLocalArea(fluff[0].getPos());
//  fluff[0].showLocalArea();
//  fluff[1].showLocalArea();
    
    
  fluff[0].changeColor(color(255,96,96,255));
  fluff[1].changeColor(color(96,96,255,255));
  fluff[2].changeColor(color(96,255,96,255));
    
  fluff[0].toScreen(4);
  fluff[1].toScreen(4);
  fluff[2].toScreen(4);
    
  fluff[0].colorFluffsInLocalArea(fluff[0].getColor());
  fluff[1].colorFluffsInLocalArea(fluff[1].getColor());
  fluff[2].colorFluffsInLocalArea(fluff[2].getColor());
  
  int[] excepList={0,1,2};
  fadeFluffsToExcept(color(0,0,0,64),excepList,0.2);
  drawFluffs(0);
  //colorFluffs(color(255,255,255));
  
}



double timeInSeconds() {
  return System.nanoTime()/1000000000.0;
}
