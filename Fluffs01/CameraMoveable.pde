class CameraMoveable {
  Vec[] eyes=new Vec[3];
  Vec center=new Vec();
  Vec upaxis=new Vec(0,1,0);
  double hiMax=1.0;
  
  CameraMoveable(double cX, double cY, double cZ, double distance) {
    initPerspective();
    hiMax=cY;
    eyes[0]=new Vec(0,0,distance);
    eyes[1]=new Vec(-2,0,distance);
    eyes[2]=new Vec(2,0,distance);
    center=new Vec(cX/2.0,cY/2.0,cZ/2.0);
  }
  
  
  CameraMoveable(double[] cen, double distance) {
    initPerspective();
    hiMax=cen[2];
    eyes[0]=new Vec(0,0,distance);
    eyes[1]=new Vec(-2,0,distance);
    eyes[2]=new Vec(2,0,distance);
    center=new Vec(cen[0]/2.0,cen[1]/2.0,cen[2]/2.0);
  }
  



  void initPerspective() {
    float fov=PI/3.0; //PI/2.3;
    float aspect=width/(float)height;
    float cameraZ = ((height/2.0) / tan(PI*60.0/360.0));
    float zNear=cameraZ/10.0;
    float zFar=cameraZ*10.0;
    perspective(fov, aspect, zNear, zFar);
  }
  
      
  void update() {
    if (mousePressed) {
      double rd=2*Math.PI*(mouseX-pmouseX)/(float)width;
      eyes[0].rotY(rd);
      eyes[1].rotY(rd);
      eyes[2].rotY(rd);
      double hiOff=-2*hiMax*(mouseY-pmouseY)/(float)height;
      eyes[0].y+=hiOff;
    }
  }
  
  
  
  void make(int eyeNr) {
    vecCamera(vecAdd(center,eyes[eyeNr]),center,upaxis);
  }  
  
}
