class SpaceData {
  double mX=1, mY=1, mZ=1;
  int divX=1, divY=1, divZ=1;
  float radius=0.1;
  
  Vector[][][] spaceHolder;
  
  SpaceData(double[] xyz, float _radius) {
    mX=xyz[0];
    mY=xyz[1];
    mZ=xyz[2];
    radius=_radius;
    divX=(int)(mX/radius);
    divY=(int)(mY/radius);
    divZ=(int)(mZ/radius);
    spaceHolder=new Vector[divX][divY][divZ];
    clearSpaceHolder();
  }
  
  void clearSpaceHolder() {
    for (int x=0;x<divX;x++) {
      for (int y=0;y<divY;y++) {
        for (int z=0;z<divZ;z++) {
          spaceHolder[x][y][z]=new Vector();
        }
      }
    }
  }
  
  void addToSpaceHolder(Fluff f) {
    Vec ix=posToSpaceIndex(f.getPos());
    spaceHolder[(int)ix.x][(int)ix.y][(int)ix.z].add(f);
  }
  
  void showBox(Vec v) {
    Vec spaceIndex=posToSpaceIndex(v);
    noFill();
    stroke(32,32,32);
    drawBoxAt((int)spaceIndex.x,(int)spaceIndex.y,(int)spaceIndex.z);
  }
  
  void showLocalArea(Vec v) {
    noFill();
    stroke(64,64,64);
    Vec ix=posToSpaceIndex(v);
    for (int x=-1;x<=1;x++) {
      for (int y=-1;y<=1;y++) {
        for (int z=-1;z<=1;z++) {
          int sx=(int)constrain((int)ix.x+x,0,divX-1);
          int sy=(int)constrain((int)ix.y+y,0,divY-1);
          int sz=(int)constrain((int)ix.z+z,0,divZ-1);
          drawBoxAt(sx,sy,sz);
        }
      }
    }
  }
  
  Vector fluffsInLocalArea(Vec v) {
    Vector out=new Vector();
    Vec ix=posToSpaceIndex(v);
    for (int x=-1;x<=1;x++) {
      for (int y=-1;y<=1;y++) {
        for (int z=-1;z<=1;z++) {
          int sx=(int)constrain((int)ix.x+x,0,divX-1);
          int sy=(int)constrain((int)ix.y+y,0,divY-1);
          int sz=(int)constrain((int)ix.z+z,0,divZ-1);
          int vl=spaceHolder[sx][sy][sz].size();
          if (vl>0) {
            for (int i=0;i<vl;i++) {
              Fluff temp=(Fluff)(spaceHolder[sx][sy][sz].elementAt(i));
              if (vecSub(temp.getPos(),v).len()<radius&&temp.getPos()!=v) out.add(temp);
            }
          }
          //drawBoxAt(sx,sy,sz);
        }
      }
    }
    return out;
  }
  
  Vec posToSpaceIndex(Vec v) {
    int x=constrain((int)(v.x/radius),0,divX-1);
    int y=constrain((int)(v.y/radius),0,divY-1);
    int z=constrain((int)(v.z/radius),0,divZ-1);
    Vec out=new Vec(x,y,z);
    return out;
  }
  
  void toScreen() {
    noFill();
    stroke(32,32,32);
    for (int x=0;x<divX;x++) {
      for (int y=0;y<divY;y++) {
        for (int z=0;z<divZ;z++) {
          drawBoxAt(x,y,z);
        }
      }
    }
  }
  
  void drawBoxAt(int x, int y, int z) {
    pushMatrix();
    translate((float)(radius/2.0),(float)(radius/2.0),(float)(radius/2.0));
    translate((float)(x*radius),(float)(y*radius),(float)(z*radius));
    box(radius,radius,radius);
    popMatrix();
  }
}
