
void setup() {
  size(500,500,P3D);  
  setupFleas();
}

void draw() {
  background(240);
  updateFleas();
}



////////////////////////////// fleas //////////////////////////////

int nrOfFleas=5000;
Flea[] flea;

void setupFleas() { 
  flea=new Flea[nrOfFleas]; 
  for (int i=0;i<nrOfFleas;i++) { flea[i]=new Flea(); } 
}

void updateFleas() { 
  for (int i=0;i<nrOfFleas;i++) { flea[i].update(); } 
  for (int i=0;i<nrOfFleas;i++) { flea[i].toScreen(); }
}

class Flea {
  int type=(int)random(20)+1;
  int mode=0; // 0: stop, 1:craw, 2:bounce
  Vec pos=new Vec(random(width),random(height));
  Vec mov=new Vec(random(3),random(3));
  Flea() {}
  
  void update() {
    if (mode==0) {
      Vec movAdd=new Vec(noiseRandom(-2,2,pos.x/(float)width,pos.y/float(height),type*millis()),noiseRandom(-2,2,pos.x/(float)width,pos.y/float(height),type*millis()));
      movAdd.Mul(0.01);
      mov.Add(movAdd);
      mov.Mul(0.999);
    } else if (mode==1) {
    
    } else if (mode==2) {
    
    }
    pos.Add(mov);
    pos.inScreen();
  }
  
  void toScreen() { set((int)pos.x,(int)pos.y,type*40); }
}

////////////////////////////// end fleas //////////////////////////////


////////////////////////////// Vec //////////////////////////////

class Vec {
  float x=0,y=0;
  Vec() {}
  Vec(float _x,float _y) {x=_x;y=_y;}
  void Add(Vec b) { x+=b.x; y+=b.y; }
  void Mul(float n) { x*=n; y*=n; }
  void inScreen() { 
    while (x<=0) {x+=width;} while (x>width) {x-=width;}
    while (y<=0) {y+=height;} while (y>height) {y-=height;}
  }
}
////////////////////////////// end Vec //////////////////////////////


float noiseRandom(float mi, float ma, float a, float b, float c) {
  float rnd=noise(a,b,c);  // 0..1
  rnd*=abs(mi-ma);                 // 0..range
  rnd+=mi;
  return rnd;
}
