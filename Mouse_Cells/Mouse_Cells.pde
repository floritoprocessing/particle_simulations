int maxCells=500;
Cell[] cells=new Cell[maxCells+1];

void setup() {
  size(400,400);
  background(0);
  for (int i=1;i<=maxCells;i++) {
    cells[i]=new Cell(i);
  }
}

void draw() {
  background(0);
  for (int i=1;i<=maxCells;i++) {
    cells[i].update();
  }
}

class Cell {
  int nr;           // instance number
  float x,y;        // position
  float lx,ly;      // last position
  float distToMouse;// distance to mouse
  float dampfac;    // damping
  float xmov,ymov;  // movemenet vector
  boolean toward;
  color white=color(255,255,255);
  Cell(int n) {
    nr=n;
    x=random(width); y=random(height); lx=x; ly=y;
    xmov=random(-2,2);
    ymov=random(-2,2);
  }
  
  void update() {
    distToMouse=dist(x,y,mouseX,mouseY);
    if (dist(lx,ly,mouseX,mouseY)>distToMouse) {toward=true;} else {toward=false;}
    if (x>width || x<1) {xmov*=-1;}
    if (y>height || y<1) {ymov*=-1;}
    if (toward==false) {
      if (distToMouse<25) {
        if (dist(0,0,xmov,ymov)>0.1) {
          dampfac=0.98;
          xmov*=dampfac; ymov*=dampfac;
        }
      }
    } else {
    
    }
    x+=xmov; y+=ymov;
    set(int(x),int(y),white);
  }
}
