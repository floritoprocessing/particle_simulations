import processing.core.*;

public class DrawingSurface extends PImage {

	public static final int MODE_NORMAL = 0;

	public static final int MODE_ADD = 1;

	private int MODE = MODE_NORMAL;

	private static final int MAX_VALUE = 255;

	DrawingSurface(int w, int h) {
		super(w, h);
	}

	public void setMode(int mode) {
		MODE = mode;
	}

	private int red(int c) {
		return c >> 16 & 0xFF;
	}

	private int green(int c) {
		return c >> 8 & 0xFF;
	}

	private int blue(int c) {
		return c & 0xFF;
	}

	private int colorMix(int cDraw, int cBack, double pDraw) {
		if (MODE == MODE_NORMAL) {
			double pBack = 1.0 - pDraw;
			int r = (int) (pBack * red(cBack) + pDraw * red(cDraw));
			int g = (int) (pBack * green(cBack) + pDraw * green(cDraw));
			int b = (int) (pBack * blue(cBack) + pDraw * blue(cDraw));
			return color(r, g, b);
		} else if (MODE == MODE_ADD) {
			int r = (int) (red(cBack) + pDraw * red(cDraw));
			int g = (int) (green(cBack) + pDraw * green(cDraw));
			int b = (int) (blue(cBack) + pDraw * blue(cDraw));
			r = (r > MAX_VALUE ? MAX_VALUE : r);
			g = (g > MAX_VALUE ? MAX_VALUE : g);
			b = (b > MAX_VALUE ? MAX_VALUE : b);
			return color(r, g, b);
		} else {
			return 0;
		}
	}

	private int color(int r, int g, int b) {
		return r << 16 | g << 8 | b;
	}

	public void set(int x, int y, int c1, double p1) {
		int c0 = get(x, y);
		set(x, y, colorMix(c1, c0, p1));
	}

	public void set(double x, double y, int c, double p) {
		if (x < 0)
			return;
		else if (x >= width)
			return;
		else if (y < 0)
			return;
		else if (y >= height)
			return;
		int x0 = (int) x, x1 = x0 + 1;
		int y0 = (int) y, y1 = y0 + 1;
		double px1 = x - x0, px0 = 1.0 - px1;
		double py1 = y - y0, py0 = 1.0 - py1;
		double p00 = p * px0 * py0;
		double p10 = p * px1 * py0;
		double p01 = p * px0 * py1;
		double p11 = p * px1 * py1;
		set(x0, y0, c, p00);
		set(x1, y0, c, p10);
		set(x0, y1, c, p01);
		set(x1, y1, c, p11);
	}

	public void line(double x0, double y0, double x1, double y1, int c, double p) {
		double dx = x1-x0, dy = y1-y0;
		double lenSq = dx*dx + dy*dy;
		double step = 1.0/Math.sqrt(lenSq);
		if (lenSq>1) {
			for (double fp=0;fp<1.0;fp+=step) {
				set(x0+fp*dx,y0+fp*dy,c,p);
			}
		} else {
			set(x0,y0,c,p);
		}
	}
}
