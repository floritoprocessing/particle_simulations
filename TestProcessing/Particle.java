public class Particle {
	
	DrawingSurface parent;
	private double speed, direction;
	private double pX, pY;
	private double X, Y;
	private double XM, YM;
	private int COLOR = 0xFF5F1A;
	private double PRESSURE = 0.1;
	
	Particle(DrawingSurface pa, double x, double y) {
		parent = pa;
		X = x;
		Y = y;
		speed = 1.0+1.0*Math.random();
		direction = 2 * Math.PI * Math.random();
		makeMoveVector();
	}
	
	public void setColor(int c) {
		COLOR = c;
	}
	public void setPressure(double p) {
		PRESSURE = p;
	}
	private void makeMoveVector() {
		XM = speed*Math.cos(direction);
		YM = speed*Math.sin(direction);
	}
	
	public void changeDirection() {
		direction += -1.0 + 2.0 * Math.random();
		makeMoveVector();
	}
	
	public void move() {
		pX = X;
		pY = Y;
		X += XM;
		Y += YM;
		if (X>=parent.width) {
			X-=parent.width;
			pX-=parent.width;
		}
		else if (X<0) {
			X+=parent.width;
			pX+=parent.width;
		}
		if (Y>=parent.height) {
			Y-=parent.height;
			pY-=parent.height;
		}
		else if (Y<0) {
			Y+=parent.height;
			pY+=parent.height;
		}
	}
	
	public void draw() {
		parent.line(pX,pY,X,Y,COLOR,PRESSURE);
	}
	
}
