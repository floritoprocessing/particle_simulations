import processing.core.*;

public class MyProcessingSketch extends PApplet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int PARTICLE_AMOUNT = 5000;
	int PARTICLE_BLACK_AMOUNT = 5000;
	
	Particle[] particle;
	DrawingSurface drawingSurface;
	
	public static void main(String[] args) {
		PApplet.main("MyProcessingSketch");
	}
	
	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		loop();
		drawingSurface = new DrawingSurface(width,height);
		drawingSurface.setMode(DrawingSurface.MODE_ADD);
		particle = new Particle[PARTICLE_AMOUNT+PARTICLE_BLACK_AMOUNT];
		for (int i=0;i<particle.length;i++) particle[i] = new Particle(drawingSurface, random(width),random(height));
		for (int i=0;i<PARTICLE_BLACK_AMOUNT;i++) particle[i].setColor(0x000000);
	}

	
	public void draw() {
		long ms = millis();
		
		double pFac = (double)mouseX/width;
		if (pFac<0) pFac=0; else if (pFac>1) pFac=1;
		double pFac2 = 1.0-pFac;
		double yFac = (double)mouseY/height;
		if (yFac<0) yFac=0; else if (yFac>1) yFac=1;
		pFac *= yFac;
		pFac2 *= yFac;
		
		for (int i=0;i<PARTICLE_BLACK_AMOUNT;i++) particle[i].setPressure(pFac);
		for (int i=PARTICLE_BLACK_AMOUNT;i<particle.length;i++) particle[i].setPressure(pFac2);
		
		for (int i=0;i<particle.length;i++) particle[i].changeDirection();
		for (int i=0;i<particle.length;i++) particle[i].move();
		
		drawingSurface.setMode(DrawingSurface.MODE_NORMAL);
		for (int i=0;i<PARTICLE_BLACK_AMOUNT;i++) particle[i].draw();
		drawingSurface.setMode(DrawingSurface.MODE_ADD);
		for (int i=PARTICLE_BLACK_AMOUNT;i<particle.length;i++) particle[i].draw();
		
		image(drawingSurface,0,0);
		println(millis()-ms);
	}
	
}
