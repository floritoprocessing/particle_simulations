class FluffTail {
  
  Vec[] pos;
  float rr,gg,bb,aa;
  int nrOfTraces;
  int traceLen=0;
  
  FluffTail(int _nrOfTraces, color _col) {
    nrOfTraces=_nrOfTraces;
    rr=red(_col);
    gg=green(_col);
    bb=blue(_col);
    aa=alpha(_col);
    pos=new Vec[nrOfTraces];
    for (int i=0;i<nrOfTraces;i++) {
      pos[i]=new Vec();
    }
  }
  
  void update(Vec _pos, color dc) {
    rr=red(dc);
    gg=green(dc);
    bb=blue(dc);
    aa=alpha(dc);
    // shift right!
    if (traceLen>0) {
      for (int i=traceLen-1;i>0;i--) {
        pos[i].setVec(pos[i-1]);
      }
    }
    
    pos[0].setVec(_pos);

    for (int i=0;i<traceLen;i++) {
      stroke(rr,gg,bb,aa*(traceLen-i)/(float)traceLen);
      vecPoint(pos[i]);
    }
    
    if (traceLen<nrOfTraces) traceLen++;
  }
  
}
