Fluff[] fluff=new Fluff[100];

void setup() {
  size(720,576,P3D);
  initFluffs(width,height,width);
  startFluffs(timeInSeconds());
}

void draw() {
  background(240,240,240);
  updateFluffs(timeInSeconds());

  camera();
  drawFluffs();
}


double timeInSeconds() {
  return System.nanoTime()/1000000000.0;
}
