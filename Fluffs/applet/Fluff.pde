class Fluff {

  // DISPLAY PRESETS:
  // ----------------
  color bodyColor=color(128,0,0);
  color tailColor=color(0,0,0);
  float bodySize=2;


  // BEHAVIOR PRESETS:
  // -----------------
  double PUSH_FREQUENCY=1;
  double PUSH_STRENGTH_MAX=0.05;
  double PUSH_PHASE=2*Math.PI*Math.random();
  double MOVE_ROT_MAX=2*Math.PI/10.0;
  double SLIPPERYNESS=0.95;


  // INTERNAL VARIABLES:
  double maxX=1, maxY=1, maxZ=1;
  Vec bodyPos=new Vec();
  Vec pushDir=rndDirVec(1.0);
  Vec moveVec=new Vec();
  float pushStrength=0;
  double phaseNow=0;
  double lastSec=0;
 
  
  // CONSTRUCTOR:
  Fluff(double _mX, double _mY, double _mZ) {
    maxX=_mX;
    maxY=_mY;
    maxZ=_mZ;
  }
  
  
  // METHOD TO GIVE RANDOM POSITION
  void randomizePos() {
    bodyPos=new Vec(maxX*Math.random(),maxY*Math.random(),-maxZ*Math.random());
  }
  
  
  // METHOD TO START COUNTING TIME
  void start(double nowSec) {
    lastSec=nowSec;
  }
  
  
  // UPDATE POSITION
  void update(double nowSec) {
    // calculate time between this and last frame:
    double secAdvance=nowSec-lastSec;
    lastSec=nowSec;
    
    // make phase:
    phaseNow+=PUSH_FREQUENCY*2*Math.PI*secAdvance;
    
    // if phase goes through zero, turn direction
    if (phaseNow+PUSH_PHASE>2*Math.PI) {
      phaseNow-=2*Math.PI;
      pushDir.rotX(MOVE_ROT_MAX);
      pushDir.rotY(MOVE_ROT_MAX);
    }
    
    double amp=PUSH_STRENGTH_MAX*(0.5+0.5*Math.sin(phaseNow+PUSH_PHASE));
    moveVec.add(vecMul(pushDir,amp));
    moveVec.mul(SLIPPERYNESS);
    bodyPos.add(moveVec);
    
    bodyPos.constrXYZ(0,maxX,0,maxY,-maxZ,0);
  }
  
  
  // DRAW ON SCREEN
  void toScreen() {
    pushMatrix();
    vecTranslate(bodyPos);
    noStroke();
    fill(bodyColor);
    ellipseMode(CENTER_RADIUS);
    ellipse(0,0,bodySize,bodySize);
    popMatrix();
  }
}







void initFluffs(int x, int y, int z) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i]=new Fluff(x,y,z);
    fluff[i].randomizePos();
  }
}

void startFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].start(nowSec);
  }
}

void updateFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].update(nowSec);
  }
}

void drawFluffs() {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].toScreen();
  }
}
