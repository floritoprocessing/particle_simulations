import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class Fluffs extends PApplet {Fluff[] fluff=new Fluff[100];

public void setup() {
  size(720,576,P3D);
  initFluffs(width,height,width);
  startFluffs(timeInSeconds());
}

public void draw() {
  background(240,240,240);
  updateFluffs(timeInSeconds());

  camera();
  drawFluffs();
}


public double timeInSeconds() {
  return System.nanoTime()/1000000000.0f;
}
class Fluff {

  // DISPLAY PRESETS:
  // ----------------
  int bodyColor=color(128,0,0);
  int tailColor=color(0,0,0);
  float bodySize=2;


  // BEHAVIOR PRESETS:
  // -----------------
  // 1 0.05
  double PUSH_FREQUENCY=1+Math.random();
  double PUSH_STRENGTH_MAX=0.05f;
  double PUSH_PHASE=2*Math.PI*Math.random();
  double MOVE_ROT_MAX=2*Math.PI/10.0f;
  double SLIPPERYNESS=0.95f;


  // INTERNAL VARIABLES:
  double maxX=1, maxY=1, maxZ=1;
  Vec bodyPos=new Vec();
  Vec pushDir=rndDirVec(1.0f);
  Vec moveVec=new Vec();
  float pushStrength=0;
  double phaseNow=0;
  double lastSec=0;
 
  
  // CONSTRUCTOR:
  Fluff(double _mX, double _mY, double _mZ) {
    maxX=_mX;
    maxY=_mY;
    maxZ=_mZ;
  }
  
  
  // METHOD TO GIVE RANDOM POSITION
  public void randomizePos() {
    bodyPos=new Vec(maxX*Math.random(),maxY*Math.random(),-maxZ*Math.random());
  }
  
  
  // METHOD TO START COUNTING TIME
  public void start(double nowSec) {
    lastSec=nowSec;
  }
  
  
  // UPDATE POSITION
  public void update(double nowSec) {
    // calculate time between this and last frame:
    double secAdvance=nowSec-lastSec;
    lastSec=nowSec;
    
    // make phase:
    phaseNow+=PUSH_FREQUENCY*2*Math.PI*secAdvance;
    
    // if phase goes through zero, turn direction
    if (phaseNow+PUSH_PHASE>2*Math.PI) {
      phaseNow-=2*Math.PI;
      pushDir.rotX(MOVE_ROT_MAX);
      pushDir.rotY(MOVE_ROT_MAX);
    }
    
    double amp=PUSH_STRENGTH_MAX*(0.5f+0.5f*Math.sin(phaseNow+PUSH_PHASE));
    moveVec.add(vecMul(pushDir,amp));
    moveVec.mul(SLIPPERYNESS);
    bodyPos.add(moveVec);
    
    bodyPos.constrXYZ(0,maxX,0,maxY,-maxZ,0);
  }
  
  
  // DRAW ON SCREEN
  public void toScreen() {
    pushMatrix();
    vecTranslate(bodyPos);
    noStroke();
    fill(bodyColor);
    ellipseMode(CENTER_RADIUS);
    ellipse(0,0,bodySize,bodySize);
    popMatrix();
  }
}







public void initFluffs(int x, int y, int z) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i]=new Fluff(x,y,z);
    fluff[i].randomizePos();
  }
}

public void startFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].start(nowSec);
  }
}

public void updateFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].update(nowSec);
  }
}

public void drawFluffs() {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].toScreen();
  }
}

/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double x=0,y=0,z=0;
  
  Vec() {
  }

  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  public void setVec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  public double len() {
    return Math.sqrt(x*x+y*y+z*z);
  }
  
  public double lenSQ() {
    return (x*x+y*y+z*z);
  }
  
  public void gravitateTo(Vec _to, double _mgd, double _fFac) {
    double minGravDist2=_mgd*_mgd;
    double dx=_to.x-x;
    double dy=_to.y-y;
    double dz=_to.z-z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    add(new Vec(dx*F,dy*F,dz*F));
  }

  public void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }
  
  public void add(double _x, double _y, double _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  public void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  public void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }
  
  public void div(double p) {
    x/=p;
    y/=p;
    z/=p;
  }

  public void negX() {
    x=-x;
  }

  public void negY() {
    y=-y;
  }

  public void negZ() {
    z=-z;
  }

  public void neg() {
    negX();
    negY();
    negZ();
  }
  
  public void normalize() {
    double l=len();
    if (l!=0) { // if not nullvector
      div(l);
    }
  }
  
  public Vec getNormalized() {
    double l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } else {
      return new Vec();
    }
  }
  
  public void setLen(double ml) {
    Vec out=new Vec(this);
    double l=len();
    if (l!=0) {
      double fac=ml/len();
      x*=fac;
      y*=fac;
      z*=fac;
    }
  }
  
  public void toAvLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/((ml+len())/2.0f);
    x*=fac;
    y*=fac;
    z*=fac;
  }

  public void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  public void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  public void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  public void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
  
  public boolean isNullVec() {
    if (x==0&&y==0&&z==0) {return true;} else {return false;}
  }
  
  public void constrX(double mi, double ma) {
    double ra=ma-mi;
    while (x<mi) x+=ra;
    while (x>=ma) x-=ra;
  }
  public void constrY(double mi, double ma) {
    double ra=ma-mi;
    while (y<mi) y+=ra;
    while (y>=ma) y-=ra;
  }
  public void constrZ(double mi, double ma) {
    double ra=ma-mi;
    while (z<mi) z+=ra;
    while (z>=ma) z-=ra;
  }
  public void constrXYZ(double xmi, double xma, double ymi, double yma, double zmi, double zma) {
    constrX(xmi,xma);
    constrY(ymi,yma);
    constrZ(zmi,zma);
  }
}




public Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

public Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

public Vec vecMul(Vec a, double b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

public Vec vecDiv(Vec a, double b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

public double vecLen(Vec a) {
  return a.len();
}

public Vec vecRotY(Vec a, double rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

public Vec rndDirVec(double r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0f,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

public Vec rndPlusMinVec(double s) {
  Vec out=new Vec(-s+2*s*Math.random(),-s+2*s*Math.random(),-s+2*s*Math.random());
  return out;
}

public void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.setVec(v2);
  v2.setVec(t);
}

/*****************************
 * adapted 3d functions to Vec
 *****************************/

public void vecTranslate(Vec v) {
  translate((float)v.x,(float)v.y,(float)v.z);
}

public void vecVertex(Vec v) {
  vertex((float)v.x,(float)v.y,(float)v.z);
}

public void vecLine(Vec a, Vec b) {
  line((float)a.x,(float)a.y,(float)a.z,(float)b.x,(float)b.y,(float)b.z);
}

public void vecRect(Vec a, Vec b) {
  rect((float)a.x,(float)a.y,(float)b.x,(float)b.y);
}

public void vecCamera(Vec eye, Vec cen, Vec upaxis) {
  camera((float)eye.x,(float)eye.y,(float)eye.z,(float)cen.x,(float)cen.y,(float)cen.z,(float)upaxis.x,(float)upaxis.y,(float)upaxis.z);
}
}