import java.util.Vector;


// PRESET VARIABLES:
int nrOfFluffs=300;
double[] ALL_AREA_SIZE={300,300,300};
boolean mode3D=false;

// internal variables:
Fluff[] fluff;
CameraMoveable myCamera;
SpaceData spaceData;



void setup() {
  size(720,576,P3D);
  colorMode(RGB,255);
  myCamera=new CameraMoveable(ALL_AREA_SIZE,300);
  initFluffs(ALL_AREA_SIZE);
  spaceData=new SpaceData(ALL_AREA_SIZE,100);
  
  startFluffs(timeInSeconds());
}



void draw() {
  background(0,0,0);
  updateFluffs(timeInSeconds());
  
  spaceData.clearSpaceHolder();
  for (int i=0;i<fluff.length;i++) {
    spaceData.addToSpaceHolder(fluff[i]);
  }
  
  
  myCamera.update();
  
  if (mode3D) {
    myCamera.make(1);
    drawFluffs(1);
    myCamera.make(2);
    drawFluffs(2);
  } else {
    myCamera.make(0);
    
    //spaceData.toScreen();
    //spaceData.showLocalArea(fluff[0].getPos());
    fluff[0].showLocalArea();
    fluff[1].showLocalArea();
    
    
    fluff[0].changeColor(color(255,64,64));
    fluff[1].changeColor(color(128,128,255));
    
    fluff[0].toScreen(4);
    fluff[1].toScreen(4);
    
    fluff[0].colorFluffsInLocalArea(color(255,64,64));
    fluff[1].colorFluffsInLocalArea(color(128,128,255));
    
    drawFluffs(0);
    
    
  }
  
  //colorFluffs(color(255,255,255));
  
}



double timeInSeconds() {
  return System.nanoTime()/1000000000.0;
}
