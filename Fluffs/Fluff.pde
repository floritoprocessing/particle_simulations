class Fluff {

  // DISPLAY PRESETS:
  // ----------------
  color DISP_BODY_COLOR=color(255,255,255);
  int DISP_TAIL_LENGTH=10;
  boolean DISP_TAIL_SHOW=true;
  
  // BEHAVIOR PRESETS:
  // -----------------
  // 1 0.05
  double PUSH_FREQUENCY=1+2*Math.random();
  double PUSH_STRENGTH_MAX=0.05+0.1*Math.random();
  double PUSH_PHASE=2*Math.PI*Math.random();
  float  MOVE_ROT_MAX=2*PI/12.0;
  double SLIPPERYNESS=0.93;



  // INTERNAL VARIABLES:
  double maxX=1, maxY=1, maxZ=1;
  Vec bodyPos=new Vec();
  Vec pushDir=rndDirVec(1.0);
  Vec moveVec=new Vec();
  float pushStrength=0;
  double phaseNow=0;
  double lastSec=0;
  double amp=0;
 
  FluffTail fluffTail;
 
  
  // CONSTRUCTOR:
  Fluff(double _mX, double _mY, double _mZ) {
    maxX=_mX;
    maxY=_mY;
    maxZ=_mZ;
  }
  
  
  
  // METHOD TO GIVE RANDOM POSITION
  void randomizePos() {
    bodyPos=new Vec(maxX*Math.random(),maxY*Math.random(),maxZ*Math.random());
    fluffTail=new FluffTail(DISP_TAIL_LENGTH,DISP_BODY_COLOR);
  }
  
  
  
  // METHOD TO START COUNTING TIME
  void start(double nowSec) {
    lastSec=nowSec;
  }
  
  
  
  Vec getPos() {
    return bodyPos;
  }
  
  
  void showTail() {
    DISP_TAIL_SHOW=true;
  }
  void hideTail() {
    DISP_TAIL_SHOW=false;
  }
  
  void changeColor(color col) {
    DISP_BODY_COLOR=col;
  }
  
  
  
  void colorFluffsInLocalArea(color c) {
    Vector listOfNeighbours=spaceData.fluffsInLocalArea(bodyPos);
    for (int i=0;i<listOfNeighbours.size();i++) {
      Fluff temp=(Fluff)(listOfNeighbours.elementAt(i));
      temp.changeColor(c);
    }
    //println(listOfNeighbours.size());
  }
  
  
  
  
  // UPDATE POSITION
  void update(double nowSec) {
    // calculate time between this and last frame:
    double secAdvance=nowSec-lastSec;
    lastSec=nowSec;
    
    // make phase:
    phaseNow+=PUSH_FREQUENCY*2*Math.PI*secAdvance;
    
    // if phase goes through zero, turn direction
    if (phaseNow+PUSH_PHASE>2*Math.PI) {
      phaseNow-=2*Math.PI;
      
      if (bodyPos.outRangeXYZ(0,maxX,0,maxY,0,maxZ)) {
        pushDir=new Vec(vecSub(bodyPos,new Vec(maxX/2.0,maxY/2.0,maxZ/2.0)));
        pushDir.normalize();
        pushDir.mul(-1);
      } else {
        pushDir.rot(random(-MOVE_ROT_MAX,MOVE_ROT_MAX),random(-MOVE_ROT_MAX,MOVE_ROT_MAX),random(-MOVE_ROT_MAX,MOVE_ROT_MAX));
      }
    }
    
    double amp=0.5+0.5*Math.sin(phaseNow+PUSH_PHASE);  // 0..1
    if (amp>0.85) {amp=1;} else {amp=0;}
    //amp=amp*amp*amp;
    moveVec.add(vecMul(pushDir,PUSH_STRENGTH_MAX*amp));
    moveVec.mul(SLIPPERYNESS);
    bodyPos.add(moveVec);
  }
  
  
  
  // DRAW ON SCREEN
  void toScreen(int mode) {
    color dc=color(0,0,0);
    if (mode==0||mode==4) dc=DISP_BODY_COLOR;
    if (mode==1) dc=color(255,0,0,255);
    if (mode==2) dc=color(0,255,0,255);
    stroke(dc);
    vecPoint(bodyPos);
    if (mode==4) {
      pushMatrix();
      fill(DISP_BODY_COLOR);
      stroke(DISP_BODY_COLOR);
      sphereDetail(8);
      vecTranslate(bodyPos);
      sphere(1);
      popMatrix();
    }
    if (DISP_TAIL_SHOW) fluffTail.update(bodyPos,dc);
  }
  
  
  void showLocalArea() {
    pushMatrix();
    noFill();
    stroke(64,64,64,128);
    sphereDetail(8);
    vecTranslate(bodyPos);
    sphere((float)spaceData.radius);
    popMatrix();
  }
  
  
}







void initFluffs(double[] xyz) {
  fluff=new Fluff[nrOfFluffs];
  for (int i=0;i<fluff.length;i++) {
    fluff[i]=new Fluff(xyz[0],xyz[1],xyz[2]);
    fluff[i].randomizePos();
  }
}

void startFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].start(nowSec);
  }
}

void updateFluffs(double nowSec) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].update(nowSec);
  }
}

void colorFluffs(color c) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].changeColor(c);
  }
}

void drawFluffs(int mode) {
  for (int i=0;i<fluff.length;i++) {
    fluff[i].toScreen(mode);
  }
}
