package graf.alive;

public class Planet {
	
	private float x, y;
	private float xm, ym;
	
	private float mass = 1;
	
	public Planet(float x, float y) {
		this.x=x;
		this.y=y;
	}
	
	public Planet(float x, float y, float mass) {
		this(x,y);
		this.mass=mass;
	}

	public void setPos(float x, float y) {
		this.x=x;
		this.y=y;
	}
	
	public void setMov(float xm, float ym) {
		this.xm=xm;
		this.ym=ym;
	}
	
	public void drag(float fac, float time) {
		double f = Math.pow(fac,time);
		xm *= f;
		ym *= f;
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public void accelerate(float xa, float ya, float time) {
		float t2 = 0.5f*time*time;
		float nx = x + time*xm + t2*xa;
		float ny = y + time*ym + t2*ya;
		xm = (nx-x)/time;
		ym = (ny-y)/time;
		x=nx;
		y=ny;
	}

	public float getMass() {
		return mass;
	}

	public void setMass(float mass) {
		this.mass = mass;
	}
	
}
