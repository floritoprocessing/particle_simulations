package graf.alive;

import processing.core.*;
import processing.graf.ColorFunctions;
import static processing.graf.ColorFunctions.*;

public class Alive extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.alive.Alive"});
	}
	
	//private final double s = 500;
	//private double rd = 0;
	//private double r = s;
	
	
	private Shaper shaper = new Shaper(Shaper.Type.SIN);
	
	private Planet[] P;
	private Osc[] pOscX;
	private Osc[] pOscY;
	
	private Planet[] Q;
	
	private float time = 1f;
	
	public void settings() {
		size(600,600,P3D);
	}
	
	public void setup() {
		
		background(0);
		ColorFunctions.setBlendmode(Blendmode.ADD);
		
		initP();
		
		Q = new Planet[200000];
		for (int i=0;i<Q.length;i++)
			Q[i] = new Planet(random(width),random(height),1.0f);
	}
	
	private void initP() {
		P = new Planet[1];
		pOscX = new Osc[P.length];
		pOscY = new Osc[P.length];
		
		for (int i=0;i<P.length;i++) {
			P[i] = new Planet(width/2,height/2,400);
			pOscX[i] = new Osc(random(0.3f,0.5f),random(1.0f));
			pOscY[i] = new Osc(random(0.3f,0.5f),random(1.0f));
		}
	}
	
	public void mousePressed() {
		initP();
	}
	
	public void draw() {
		
		for (int i=0;i<P.length;i++) {
			P[i].setPos(
					width/2+200  * shaper.transform(pOscX[i]), 
					height/2+200 * shaper.transform(pOscY[i]));
			pOscX[i].tick(time);
			pOscY[i].tick(time);
		}
		
		float dx, dy, d2, d;
		float a, afac, ax, ay;
		for (int i=0;i<Q.length;i++) {
			ax = 0;
			ay = 0;
			for (int j=0;j<P.length;j++) {
				dx = P[j].getX()-Q[i].getX();
				dy = P[j].getY()-Q[i].getY();
				d2 = dx*dx+dy*dy;
				d = (float)Math.sqrt(d2);
				if (d<20) d=20;
				
				a = P[j].getMass()/d2;
				afac = a/d;
				ax += dx*afac;
				ay += dy*afac;
			}
			
			Q[i].accelerate(ax, ay, time);
			//Q[i].drag(0.9f,time);
			if (Q[i].getX()<-0*width
					|| Q[i].getX()>1*width
					|| Q[i].getY()<-0*height
					|| Q[i].getY()>1*height) {
				Q[i].setPos(random(width), random(height));
				Q[i].setMov(0,0);
			}
				
		}
		
		background(0);
		
		//dot(this, P.getX(), P.getY(), 0xd02e08, 1.0f);
		for (int i=0;i<Q.length;i++) {
			dot(this, Q[i].getX(), Q[i].getY(), 0xd02e08, 1.0f);
		}
		
		//hBlur(this);
		//vBlur(this);
		
		//threshold(this,140);
		
		//((Red value X 299) + (Green value X 587) + (Blue value X 114)) / 1000
		//((Red value X 3) + (Green value X 5) + (Blue value X 1)) / 8
		
		//ellipseMode(CENTER);
		//noStroke();
		//fill(0,0,255);
		//ellipse(P.getX(), P.getY(), 20, 20);
		
		
		
		/*for (int i=0;i<10000;i++) {
			double x = width/2 + r*Math.cos(rd);
			double y = height/2 + r*Math.sin(rd);
			dot(this, (float)x, (float)y, 0xd02e08, 0.1f);
			rd += Math.pow(r,20);
			r -= 0.05;	
			if (r<-s) r+=2*s;
		}*/
		
	}

}
