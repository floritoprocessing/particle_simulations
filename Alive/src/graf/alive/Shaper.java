package graf.alive;

public class Shaper {

	public enum Type {
		SIN;
	}
	
	private Type type;
	
	public Shaper(Type type) {
		this.type = type;
	}
	
	public float transform(Osc osc) {
		switch (type) {
		case SIN: 
			return (float)(Math.sin(Math.PI*2*osc.getPhase()));
		default: throw new RuntimeException("Unimplemented type "+type);
		}
	}
	
}
