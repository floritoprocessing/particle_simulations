package graf.alive;

public class Osc {
	
	private float phaseAdd;
	private float phase;
	
	public Osc(float phaseAdd) {
		this.phaseAdd = phaseAdd;
	}
	
	public Osc(float phaseAdd, float phase) {
		this.phaseAdd = phaseAdd;
		this.phase = phase;
	}
	
	public void tick(float time) {
		phase += time*phaseAdd;
		while (phase>=1) phase-=1;
	}
	
	public float getPhase() {
		return phase;
	}
}
