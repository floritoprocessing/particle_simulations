class RandomSpread {
  
  private double out = 0.0;
  
  RandomSpread(double input, double wid) {
    out = input + wid*(Math.random()-0.5);
  }
  
  public double get() {
    return out;
  }
  
}
