class Attractor {
  
  private double x, y;
  private double xCen, yCen;
  //private double spreadWidth = 1.7;
  private double mass = 1.0;
  private double minDistance = 0.0;
  
  Attractor(double m, double md) {
    mass = m;
    minDistance = md;
    xCen = (new RandomSpread(0,ATTRACTOR_SPREAD_WIDTH)).get();
    yCen = (new RandomSpread(0,ATTRACTOR_SPREAD_HEIGHT)).get();
    x = xCen;
    y = yCen;
  }
  
  public void newBasePos() {
    xCen = (new RandomSpread(0,ATTRACTOR_SPREAD_WIDTH)).get();
    yCen = (new RandomSpread(0,ATTRACTOR_SPREAD_HEIGHT)).get();
  }
  
  public double getMass() {
    return mass;
  }
  
  public double getMinimumDistance() {
    return minDistance;
  }
  
  public void setXOff(double xo) {
    x = xCen + xo;
  }
  
  public void setYOff(double yo) {
    y = yCen + yo;
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
  
}
