/*
*  
*  class QColor is the class for storing 16-bit per channel color information
*
*  CONSTRUCTOR SUMMARY:
*  --------------------
*
*  QColor()
*    Constructs a new QColor object with the color black.
*
*  QColor(Qcolor qcolor)
*    Constructs a new QColor object with the same setting as qcolor
*
*  QColor(int red, int green, int blue)
*    Constructs a new QColor object with the channels set to the specified amounts (will always stay between 0x0000 and 0xFFFF)
*
*  QColor(double red, double green, double blue)
*    Constructs a new QColor object with the channels set to the specified amounts (will always stay between 0x0000 and 0xFFFF)
*
*
*  METHOD SUMMARY:
*  ---------------
*
*  void    set(Qcolor qcolor)
*            sets the QColor Object to the same as qcolor
*
*  void    set(int red, int green, int blue)
*            sets the QColor object channels to the specified amounts
*
*  void    set(double red, double green, double blue)
*            sets the QColor object channels to the specified amounts
*
*  int     red()
*            returns the value of the red channel (0x0000..0xFFFF)
*
*  int     green()
*            returns the value of the green channel (0x0000..0xFFFF)
*
*  int     blue()
*            returns the value of the blue channel (0x0000..0xFFFF)
*
*  color   asColor()
*            returns the color representation of this object
*
*/


class QColor  {
  
  public static final int BLACK = 0xFFFFFF;
  public static final int RED   = 0xFF0000;
  public static final int GREEN = 0x00FF00;
  public static final int BLUE  = 0x0000FF;
  public static final int WHITE = 0xFFFFFF;

  private int r, g, b;
  
  QColor() {
    r = 0;
    g = 0;
    b = 0;
  }
  
  QColor(QColor c) {
    r = c.red();
    g = c.green();
    b = c.blue();
  }
  
  QColor(int _r, int _g, int _b) {
    r = _r;
    g = _g;
    b = _b;
    boundaryCheck();
  }
  
  QColor(double _r, double _g, double _b) {
    r = (int)_r;
    g = (int)_g;
    b = (int)_b;
    boundaryCheck();
  }
  
  public void set(QColor c) {
    r = c.red();
    g = c.green();
    b = c.blue();
  }
  
  public void set(int _r, int _g, int _b) {
    r = _r;
    g = _g;
    b = _b;
    boundaryCheck();
  }
  
  public void set(double _r, double _g, double _b) {
    r = (int)_r;
    g = (int)_g;
    b = (int)_b;
    boundaryCheck();
  }
  
  private void boundaryCheck() {
    if (r<0) r=0;
    if (r>0xFFFF) r=0xFFFF;
    if (g<0) g=0;
    if (g>0xFFFF) g=0xFFFF;
    if (b<0) b=0;
    if (b>0xFFFF) b=0xFFFF;
  }
  
  public int red() {
    return r;
  }
  public int green() {
    return g;
  }
  public int blue() {
    return b;
  }
  
  public color asColor() {
    return color(r>>8&0xFF,g>>8&0xFF,b>>8&0xFF);
  }
  
}
