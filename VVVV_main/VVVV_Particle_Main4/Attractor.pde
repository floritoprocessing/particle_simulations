class Attractor {
  
  private double x = 0;
  private double y = 0;
  private double baseX = 0;
  private double baseY = 0;
  private double mass = 0;
  private double minDist = 0;
  
  LFO lfoX, lfoY;
  
  Attractor(double _mass, double md, double bp, double bpv, double amp) {
    mass = _mass;
    minDist = md;
    
    lfoX = new LFO(amp,bp+bpv*(-1+2*Math.random()));
    lfoY = new LFO(amp,bp+bpv*(-1+2*Math.random()));
    
    x = 1.7 * (-1 + 2*Math.random());
    y = 1.7 * (-1 + 2*Math.random());
    baseX = x;
    baseY = y;
  }
  
  public double getMass() {
    return mass;
  }
  
  public double getMinimumDistance() {
    return minDist;
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
  public void move(double t) {
    x = baseX + lfoX.getLFO();
    y = baseY + lfoY.getLFO();
    lfoX.stepTime(t);
    lfoY.stepTime(t);
  }
  
}
