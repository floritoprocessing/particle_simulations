import java.util.Vector;

int PARTICLE_AMOUNT = 500;
double PARTICLE_DRAG = 0.9920;
double GRAVITATIONAL_CONSTANT = 0.0001;

int ATTRACTOR_AMOUNT = 5;
double ATTRACTOR_MASS = 0.1330;
double ATTRACTOR_MINIMUM_DISTANCE = 0.0640;
double ATTRACTOR_MOV_BASE_PERIOD = 0.0890;
double ATTRACTOR_MOV_BASE_PERIOD_VAR = 0.00003;
double ATTRACTOR_MOV_AMPLITUDE = 1.04;

Vector attractors;
Vector particles;

LFO lfo;

float i=0;

void setup() {
  size(640,640,P3D);
  
  attractors = new Vector();
  for (int i=0;i<ATTRACTOR_AMOUNT;i++) {
    Attractor a = new Attractor(ATTRACTOR_MASS,ATTRACTOR_MINIMUM_DISTANCE,ATTRACTOR_MOV_BASE_PERIOD,ATTRACTOR_MOV_BASE_PERIOD_VAR,ATTRACTOR_MOV_AMPLITUDE);
    attractors.add(a);
  }
  
  lfo = new LFO(ATTRACTOR_MOV_AMPLITUDE,ATTRACTOR_MOV_BASE_PERIOD);
  
  background(0,0,0);
}


void draw() {
  for (int i=0;i<attractors.size();i++) {
    Attractor a = (Attractor)attractors.elementAt(i);
    a.move(0.04);
    rectMode(CENTER);
    fill(255,128,192);
    rect((float)(width*(0.5+0.5*a.getX())),(float)(height*(0.5+0.5*a.getY())),5,5);
  }
}
