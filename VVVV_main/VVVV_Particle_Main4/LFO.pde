class LFO {
  
  public static final int SHAPE_SINUS = 0;
  private int SHAPE = SHAPE_SINUS;
  
  private double amplitude=0;
  private double period=1;    // in sec
  private double time = 0;    // in sec
  
  LFO(double a, double p) {
    amplitude = a;
    period = p;
    time = random(1.0);
  }
  
  public void stepTime(double t) {
    time += t;
  }
  
  public double getLFO() {
    double lfo = (1/period)*time;
    lfo = lfo%1.0;
    if (SHAPE==SHAPE_SINUS) {
      lfo = 0.5 + 0.5*Math.sin(2*Math.PI*lfo);
    }
    lfo *= amplitude;
    return lfo;
  }
  
}
