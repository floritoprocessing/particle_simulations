class Particle {
  
  private double x, y;
  private double vx, vy;
  private double drag;
  
  Particle(double d) {
    x = (new RandomSpread(0,1.0)).get();
    y = (new RandomSpread(0,1.0)).get();
    vx = (new RandomSpread(0,0.008)).get();
    vy = (new RandomSpread(0,0.008)).get();
    drag = d;
  }
  
  public void move(Vector atts) {
    double ax = 0, ay = 0;
    
    for (int i=0;i<atts.size();i++) {
      Attractor a = (Attractor)atts.elementAt(i);
      double dx = a.getX() - x;
      double dy = a.getY() - y;
      double d2 = dx*dx+dy*dy;
      double d = Math.sqrt(d2);
      if (d>a.getMinimumDistance()) {
        ax += (dx/d) * GRAVITATIONAL_CONSTANT * a.getMass() / d2;
        ay += (dy/d) * GRAVITATIONAL_CONSTANT * a.getMass() / d2;
      }
    }
    vx += ax;
    vy += ay;
    x += vx;
    y += vy;
    vx *= drag;
    vy *= drag;
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
}
