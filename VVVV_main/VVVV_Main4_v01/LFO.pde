class LFO {
  
  public static final int TYPE_NORMAL = 0;
  public static final int TYPE_SINUS = 1;
  private int TYPE = 0;
  
  double period;
  double amplitude;
  double time;
  
  LFO(double pe, double am, double ph) {
    period = pe;
    amplitude = am;
    time = ph;
    stepTime(0);
  }
  
  public void setType(int t) {
    TYPE = t;
  }
  
  public void stepTime(double t) {
    time += t/period;
    time = (time%1.0);
  }
  
  public double get() {
    double out = 0;
    if (TYPE==TYPE_NORMAL) out = amplitude * time;
    if (TYPE==TYPE_SINUS) out = amplitude * 0.5 * Math.sin(time*2.0*Math.PI);
    return out;
  }
  
}
