
// ------------------------------------------------------
// v03 settings:



int    PARTICLE_AMOUNT                     = 1000;

double GRAVITATIONAL_CONSTANT              = 0.0001;

double ATTRACTOR_MASS                      = 0.0365;
double ATTRACTOR_MIN_DISTANCE              = 0.1440;

int    ATTRACTORS_AMOUNT                   = 5;

double ATTRACTOR_MOVEMENT_PERIOD           = 0.1780;
double ATTRACTOR_MOVEMENT_PERIOD_VARIATION = 0.0026;
double ATTRACTOR_MOVEMENT_AMPLITUDE        = 1.0400;

double PARTICLE_DRAG                       = 0.9990;

int    FRAMES_TO_RECORD                    = 150;
int    REINIT_ATTRACTORS_EVERY_NTH_FRAME   = 25;

int    RENDER_FRAMES_PER_FRAME = 50;

String SAVE_NAME = "output/VVVV_Main4_v01_";
String EXTENSION = ".bmp";

// ------------------------------------------------------

boolean SAVE_FRAMES = true;

import java.util.Vector;

/*
// v01
 int ATTRACTORS_AMOUNT = 5;
 double ATTRACTOR_MASS = 0.0930;
 double ATTRACTOR_MIN_DISTANCE = 0.1040;
 double ATTRACTOR_MOVEMENT_PERIOD = 0.0890;
 double ATTRACTOR_MOVEMENT_PERIOD_VARIATION = 0.0013;
 double ATTRACTOR_MOVEMENT_AMPLITUDE = 1.0400;
 
 int PARTICLE_AMOUNT = 500;
 double PARTICLE_DRAG = 0.9920;
 double GRAVITATIONAL_CONSTANT = 0.0001;
 */



QColor COLOR_BACKGROUND = new QColor(0, 0, 0);

QUtil qu;
QImage img;

Vector lfos;
Vector attractors;
Vector particles;

int frameNumber;

boolean showAttractors = true;

int FC = 0;

void setup() {
  size(400, 400, P3D);

  qu = new QUtil();
  qu.setBlendMode(QUtil.BLEND_MODE_ADD);

  img = new QImage(width, height);
  img.background(COLOR_BACKGROUND);

  // CREATE ATTRACTORS AND LFOS
  attractors = new Vector();
  lfos = new Vector();
  for (int i=0; i<ATTRACTORS_AMOUNT; i++) {
    attractors.add(new Attractor(ATTRACTOR_MASS, ATTRACTOR_MIN_DISTANCE));
    for (int j=0; j<2; j++) {
      double lfoPer = (new RandomSpread(ATTRACTOR_MOVEMENT_AMPLITUDE, ATTRACTOR_MOVEMENT_PERIOD_VARIATION)).get();
      LFO lfo = new LFO(lfoPer, ATTRACTOR_MOVEMENT_AMPLITUDE, Math.random());
      lfo.setType(LFO.TYPE_SINUS);
      lfos.add(lfo);
    }
  }

  // CREATE PARTICLES:
  particles = new Vector();
  for (int i=0; i<PARTICLE_AMOUNT; i++) {
    particles.add(new Particle(PARTICLE_DRAG));
  }

  if (SAVE_FRAMES) println("** SAVING FRAMES ENABLES! **");
  frameNumber = 0;
}

void keyPressed() {
  if (key=='a') showAttractors = !showAttractors;
}

void draw() {

  
  for (int renderPass=0; renderPass<RENDER_FRAMES_PER_FRAME; renderPass++) {

    // UPDATE LFO:
    for (int i=0; i<lfos.size(); i++) ((LFO)lfos.elementAt(i)).stepTime(0.04);



    // UPDATE POSITION OF ATTRACTORS:
    for (int i=0; i<attractors.size(); i++) {
      ((Attractor)attractors.elementAt(i)).setXOff(((LFO)lfos.elementAt(i*2+0)).get());
      ((Attractor)attractors.elementAt(i)).setYOff(((LFO)lfos.elementAt(i*2+1)).get());
    }


    // UPDATE POSITION OF PARTICLES:
    for (int i=0; i<particles.size(); i++) {
      ((Particle)particles.elementAt(i)).move(attractors);
    }


    // DRAW PARTICLES:
    QColor col = new QColor(0xFFFF, 0.48*0xFFFF, 0.16*0xFFFF);
    for (int i=0; i<particles.size(); i++) {
      Particle p = (Particle)particles.elementAt(i);
      qu.set(img, (float)VxToSx(p.getX()), (float)VyToSy(p.getY()), col, 0.1);
      //rect(VxToSx(p.getX()),VyToSy(p.getY()),0.5,0.5);
    }


    // SHOW IMAGE:
    image(img.asPImage(), 0, 0);


    if (FC%FRAMES_TO_RECORD==0) {

      // SAVE IT:
      if (SAVE_FRAMES) {
        String name = SAVE_NAME+nf(frameNumber, 5)+EXTENSION;//".tga";
        save(name);
        println(name+" saved");
        frameNumber++;
      }

      // CLEAR SCREEN:
      float FADE = 01.0;
      qu.setBlendMode(QUtil.BLEND_MODE_NORMAL);
      for (int x=0; x<img.width; x++) for (int y=0; y<img.height; y++) {
        qu.set(img, x, y, COLOR_BACKGROUND, FADE);
      }
      qu.setBlendMode(QUtil.BLEND_MODE_ADD);

      // REINIT ATTRACTORS:
      if (random(REINIT_ATTRACTORS_EVERY_NTH_FRAME)<1.0) {
        for (int i=0; i<attractors.size(); i++) {
          ((Attractor)attractors.elementAt(i)).newBasePos();
        }
        println("frame: "+FC+", new base pos!");
      }
    }


    // DRAW ATTRACTORS:
    if (showAttractors) {
      stroke(0, 0, 255);
      rectMode(CENTER);
      for (int i=0; i<attractors.size(); i++) {
        Attractor a = (Attractor)attractors.elementAt(i);
        rect(VxToSx(a.getX()), VyToSy(a.getY()), 5, 5);
      }
    }
    
    FC++;
  }
}
